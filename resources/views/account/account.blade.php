@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">My account</div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label for="referral_link">Referral link</label>
                            <input type="text" class="form-control" id="referral_link" value="{{ url('register') }}?ref={{ Auth::id() }}">
                        </div>

                        {!! Form::open(['url' => route('add-money')]) !!}
                        <div class="form-group">
                            <label for="money">Money</label>
                            <input type="number" name="money" class="form-control" id="money" placeholder="0" min="1">
                        </div>
                        <button type="submit" class="btn btn-primary">Add money</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
