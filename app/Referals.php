<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referals extends Model
{
    protected $table = 'referals';

    protected $fillable = ['user_id', 'refer_id'];
}
