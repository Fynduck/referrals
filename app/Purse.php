<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purse extends Model
{
    protected $table = 'purse';

    protected $fillable = ['user_id', 'money'];
}
