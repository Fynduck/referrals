<?php

namespace App\Http\Controllers\Account;

use App\Purse;
use App\Referals;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        if (Session::has('ref'))
            Session::forget('ref');
    }

    public function index()
    {
        return view('account.account');
    }

    protected function addMoney(Request $request)
    {

        $purse = Purse::create([
                'user_id' => Auth::id(),
                'money'   => $request->get('money')
            ]
        );

        if ($purse) {
            $refer = Referals::where('user_id', Auth::id())->first();
            if ($refer) {
                $percet_refer = $request->get('money') * 10 / 100;
                Purse::create([
                        'user_id' => $refer->id,
                        'money'   => $percet_refer]
                );
            }
        }

        return 'Success';
    }
}
